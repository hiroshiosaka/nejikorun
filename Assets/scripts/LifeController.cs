﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//抽象クラスとして設定
public abstract class LiftController : MonoBehaviour
{


    [SerializeField]//private変数をUnity上でpublic変数のように扱える
    private float speedX = 0;
    [SerializeField]
    private float speedY = 0;
    [SerializeField]
    private float speedZ = 0;

    private Vector3 startPos;//オブジェクトの初期位置

    //ゲッターセッター自動生成
    public Vector3 StartPos
    {
        get
        {
            return startPos;
        }

        set
        {
            startPos = value;
        }
    }

    //数学関数によるリフト操作(抽象メソッド)
    public virtual void funcLiftMove(float sx, float sy, float sz) { }


    void Start()
    {
        //初期位置の保存
        StartPos = transform.localPosition;
    }

    void Update()
    {
        //リフトを動かし続ける
        funcLiftMove(speedX, speedY, speedZ);
    }
}
